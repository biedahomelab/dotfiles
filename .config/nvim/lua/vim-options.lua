vim.cmd("syntax on")
vim.cmd("set expandtab") 
vim.cmd("set tabstop=2")
vim.cmd("set softtabstop=2")
vim.cmd("set shiftwidth=2")

vim.cmd("set cursorline")
vim.cmd("set cursorcolumn")
vim.cmd("set number relativenumber")
vim.cmd("set showcmd")
vim.cmd("set showmode")
vim.cmd("set showmatch")

vim.opt.fillchars:append { eob = " " }

vim.g.mapleader = " "

vim.keymap.set('n', '<Leader>z', 'ZZ' ,{} )
vim.keymap.set('n', '<Leader>w', ':w<CR>' ,{} )
vim.keymap.set('n', '<Leader>q', ':q!<CR>' ,{} )

vim.keymap.set('n', '<C-t>', ':tabnew<CR>', {})
vim.keymap.set('n', '<C-j>', ':tabprevious<CR>', {})
vim.keymap.set('n', '<C-k>', ':tabnext<CR>', {})
vim.keymap.set('n', '<Leader>tn', ':<CR>', {})
vim.keymap.set('n', '<Leader>sh', ':split<CR>', {})
vim.keymap.set('n', '<Leader>sv', ':vsplit<CR>', {})

vim.keymap.set('n', '<Leader>sw', ':<C-W><C-K>', {})
vim.keymap.set('n', '<Leader>ss', ':<C-W><C-J>', {})
vim.keymap.set('n', '<Leader>sa', ':<C-W><C-H>', {})
vim.keymap.set('n', '<Leader>sd', ':<C-W><C-L>', {})
