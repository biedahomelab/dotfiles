return {
	"nvimtools/none-ls.nvim",
	config = function()
		local null_ls = require("null-ls")
		null_ls.setup({
			sources = {
				null_ls.builtins.formatting.stylua,
				null_ls.builtins.formatting.prettierd,
				null_ls.builtins.formatting.biome,
				null_ls.builtins.formatting.black,
				null_ls.builtins.formatting.isort,
--				null_ls.builtins.formatting.beautysh,
				null_ls.builtins.completion.spell,
--				null_ls.builtins.diagnostics.eslint_d,
--				null_ls.builtins.diagnostics.shellcheck,
--				null_ls.builtins.diagnostics.shellharden,
			},
		})
		vim.keymap.set("n", "<leader>ff", vim.lsp.buf.format, {})
	end,
}
