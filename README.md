# Config

## GNU stow

```bash
sudo dnf install stow
```

```bash
git clone --depth 1 git@gitlab.com:biedahomelab/dotfiles.git
cd dotfiles
stow --adopt .
```

## zsh

```bash
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
```
```bash
sudo dnf install fzf -y
```
```bash
sudo rm /usr/bin/fzf
sudo ln -s ~/.fzf/bin/fzf /usr/bin/fzf
```
```bash
cp ~/.zshrc ~/.zshrc_backup
rm ~/.zshrc
cp zshrc ~/.zshrc
```

